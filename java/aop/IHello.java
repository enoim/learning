package enoim.learning.aop;

public interface IHello {
    public void hello(String name);
}